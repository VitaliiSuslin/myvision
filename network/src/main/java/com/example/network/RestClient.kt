package com.example.network

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import com.example.network.api.trainProgram.TrainProgramApi
import com.example.network.api.trainProgram.request.CreateTrainProgramRequest
import com.example.network.api.trainProgram.request.GetTrainProgramRequest
import com.example.network.api.trainProgram.request.GetTrainProgramsRequest
import com.example.network.api.trainProgram.response.CreateTrainProgramResponse
import com.example.network.api.trainProgram.response.GetTrainProgramResponse
import com.example.network.api.trainProgram.response.GetTrainProgramsResponse
import com.example.network.api.user.UserApi
import com.example.network.api.user.reposne.CreateUserResponse
import com.example.network.api.user.reposne.GetUsersResponse
import com.example.network.api.user.reposne.SignInResponse
import com.example.network.api.user.request.CreateUserRequest
import com.example.network.api.user.request.GetUsersRequest
import com.example.network.api.user.request.SignInRequest
import com.example.network.service.callAdapter.CallAdapterFactory
import com.example.network.service.callAdapter.CallAdapterWrapper
import com.example.network.service.exceptions.NoInternetException
import com.example.network.service.response.ResponseHandler
import com.google.gson.GsonBuilder
import io.reactivex.Single
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.reflect.Modifier
import java.util.concurrent.TimeUnit

class RestClient(
    private val context: Context
) :
    UserApi,
    TrainProgramApi {

    companion object {
        const val BASE_URL = "http://172.21.20.150:8181/api"
        const val CONNECT_TIMEOUT = 45_000L
        const val READ_TIMEOUT = 45_000L
        const val HEADER_AUTHORIZATION = "Authorization"
    }

    override fun createUser(request: CreateUserRequest): Single<Response<CreateUserResponse>> {
        return request(UserApi::class.java) {
            createUser(request)
        }
    }

    override fun signInUser(request: SignInRequest): Single<Response<SignInResponse>> {
        return request(UserApi::class.java) {
            signInUser(request)
        }
    }

    override fun getUsers(request: GetUsersRequest): Single<Response<GetUsersResponse>> {
        return request(UserApi::class.java) {
            getUsers(request)
        }
    }

    override fun getTrainProgram(request: GetTrainProgramRequest): Single<Response<GetTrainProgramResponse>> {
        return request(TrainProgramApi::class.java) {
            getTrainProgram(request)
        }
    }

    override fun createTrainProgram(request: CreateTrainProgramRequest): Single<Response<CreateTrainProgramResponse>> {
        return request(TrainProgramApi::class.java) {
            createTrainProgram(request)
        }
    }

    override fun getTrainPrograms(request: GetTrainProgramsRequest): Single<Response<GetTrainProgramsResponse>> {
        return request(TrainProgramApi::class.java) {
            getTrainPrograms(request)
        }
    }

    private fun retrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(formatBaseUrlEnding(BASE_URL))
            .client(
                OkHttpClient.Builder()
                    .connectTimeout(CONNECT_TIMEOUT, TimeUnit.MILLISECONDS)
                    .readTimeout(READ_TIMEOUT, TimeUnit.MILLISECONDS)
                    .addInterceptors(
                        listOfNotNull(
                            NetworkInterceptor()
                        )
                    )
                    .build()
            )
            .addConverterFactory(
                GsonConverterFactory.create(
                    GsonBuilder()
                        .excludeFieldsWithModifiers(Modifier.TRANSIENT)
                        .serializeNulls()
                        .create()
                )
            )
            .addCallAdapterFactory(CallAdapterFactory<Response<*>, Single<Response<*>>> {
                CallAdapterWrapper(
                    it
                )
            })
            .build()
    }

    private fun <S : Any, R : Response<*>> request(
        serviceClass: Class<S>,
        call: S.() -> Single<R>
    ): Single<R> {
        return call(retrofit().create(serviceClass))
            .flatMap(ResponseHandler())
    }

    private fun formatBaseUrlEnding(url: String): String = if (url.endsWith("/")) url else "$url/"

    private fun OkHttpClient.Builder.addInterceptors(interceptors: List<Interceptor>): OkHttpClient.Builder {
        return this.apply {
            interceptors.forEach {
                addInterceptor(it)
            }
        }
    }

    inner class NetworkInterceptor : Interceptor {
        @Throws(NoInternetException::class)
        override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
            return if (!isNetworkAvailable(context)) {
                throw NoInternetException()
            } else {
                chain
                    .proceed(
                        chain
                            .request()
                            .newBuilder()
                            .build()
                    )
            }
        }
    }

    /**
     * @param context valid Context
     * @return is connected or connecting
     */
    @SuppressLint("MissingPermission")
    fun isNetworkAvailable(context: Context): Boolean {
        return (context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager).activeNetworkInfo?.isConnected
            ?: false
    }
}