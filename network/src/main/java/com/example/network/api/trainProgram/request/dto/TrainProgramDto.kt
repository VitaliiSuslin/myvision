package com.example.network.api.trainProgram.request.dto

import com.google.gson.annotations.SerializedName


data class TrainProgramDto(
    @SerializedName("title")
    var title: String,

    @SerializedName("description")
    var description: String,

    @SerializedName("exercises")
    var exercises: MutableList<ExerciseDto>
)