package com.example.network.api.user.reposne.dto

import com.example.network.service.response.SelfValidator
import com.google.gson.annotations.SerializedName

data class UserDto(
    @SerializedName("user_id")
    var userId: Long,
    @SerializedName("user_name")
    var userName: String,
    @SerializedName("email")
    var email: String
) : SelfValidator {
    override fun validate(): Boolean {
        return userId <= 0 && userName.isNotBlank() && email.isNotBlank()
    }
}