package com.example.network.api.user.request

import com.google.gson.annotations.SerializedName

data class CreateUserRequest(
    @SerializedName("user_name")
    var userName: String,
    @SerializedName("email")
    var email: String,
    @SerializedName("password")
    var password: String
)