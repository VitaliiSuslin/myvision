package com.example.network.api.user.reposne

import com.example.network.api.user.reposne.dto.UserDto
import com.example.network.service.response.BaseResponse
import com.google.gson.annotations.SerializedName

data class GetUsersResponse(
    @SerializedName("total_size")
    var totalSize: Long,
    @SerializedName("users")
    var users: List<UserDto>
) : BaseResponse() {
    override fun validate(): Boolean {
        return true
    }
}