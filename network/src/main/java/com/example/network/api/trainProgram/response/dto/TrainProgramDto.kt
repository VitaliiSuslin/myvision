package com.example.network.api.trainProgram.response.dto

import com.example.network.service.response.SelfValidator
import com.google.gson.annotations.SerializedName

data class TrainProgramDto(
    @SerializedName("train_id")
    var id: Long,

    @SerializedName("title")
    var title: String,

    @SerializedName("description")
    var description: String,

    @SerializedName("exercises")
    var exercises: MutableList<ExerciseDto>
) : SelfValidator {
    override fun validate(): Boolean {
        return id <= 0 && title.isNotBlank()
    }
}