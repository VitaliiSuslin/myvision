package com.example.network.api.trainProgram.request.dto

import com.google.gson.annotations.SerializedName


data class ExerciseDto(
    @SerializedName("title")
    var title: String,

    @SerializedName("description")
    var description: String
)