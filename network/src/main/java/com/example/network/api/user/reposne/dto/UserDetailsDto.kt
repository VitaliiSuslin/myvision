package com.example.network.api.user.reposne.dto

import com.example.network.service.response.SelfValidator
import com.google.gson.annotations.SerializedName

data class UserDetailsDto(
    @SerializedName("id")
    var userId: Long,

    @SerializedName("email")
    var email: String,

    @SerializedName("name")
    var userName: String,

    @SerializedName("date_birth")
    var dateBirth: Long? = null,

    @SerializedName("weight")
    var weight: Float? = null,

    @SerializedName("height")
    var height: Float? = null
) : SelfValidator {
    override fun validate(): Boolean {
        return userId <= 0 && email.isNotBlank() && userName.isNotBlank()
    }
}