package com.example.network.api.trainProgram.request

import com.example.network.api.trainProgram.request.dto.TrainProgramDto
import com.google.gson.annotations.SerializedName


data class CreateTrainProgramRequest(
    @SerializedName("train_program")
    var trainProgram: TrainProgramDto
)