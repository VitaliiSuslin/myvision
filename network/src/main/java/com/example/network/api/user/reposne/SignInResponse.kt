package com.example.network.api.user.reposne

import com.example.network.service.response.BaseResponse
import com.google.gson.annotations.SerializedName

data class SignInResponse(
    @SerializedName("token")
    var token: String
) : BaseResponse() {
    override fun validate(): Boolean {
        return token.isNotBlank()
    }
}