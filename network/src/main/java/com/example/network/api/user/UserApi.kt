package com.example.network.api.user

import com.example.network.api.user.reposne.CreateUserResponse
import com.example.network.api.user.reposne.GetUsersResponse
import com.example.network.api.user.reposne.SignInResponse
import com.example.network.api.user.request.CreateUserRequest
import com.example.network.api.user.request.GetUsersRequest
import com.example.network.api.user.request.SignInRequest
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.POST

interface UserApi {

    companion object {
        private const val ROUT = "user"
    }

    @POST("$ROUT/create")
    fun createUser(request: CreateUserRequest): Single<Response<CreateUserResponse>>

    @POST("$ROUT/sign_in")
    fun signInUser(request: SignInRequest): Single<Response<SignInResponse>>

    @POST(ROUT)
    fun getUsers(request: GetUsersRequest): Single<Response<GetUsersResponse>>
}