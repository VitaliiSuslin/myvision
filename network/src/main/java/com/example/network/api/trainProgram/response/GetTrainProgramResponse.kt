package com.example.network.api.trainProgram.response

import com.example.network.api.trainProgram.response.dto.TrainProgramDto
import com.example.network.service.response.BaseResponse
import com.google.gson.annotations.SerializedName

data class GetTrainProgramResponse(
    @SerializedName("train_program")
    var trainProgram: TrainProgramDto
) : BaseResponse() {
    override fun validate(): Boolean {
        return trainProgram.validate()
    }
}