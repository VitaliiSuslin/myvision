package com.example.network.api.trainProgram

import com.example.network.api.trainProgram.request.CreateTrainProgramRequest
import com.example.network.api.trainProgram.request.GetTrainProgramRequest
import com.example.network.api.trainProgram.request.GetTrainProgramsRequest
import com.example.network.api.trainProgram.response.CreateTrainProgramResponse
import com.example.network.api.trainProgram.response.GetTrainProgramResponse
import com.example.network.api.trainProgram.response.GetTrainProgramsResponse
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.POST

interface TrainProgramApi {

    companion object {
        private const val ROUT = "train_program"
    }

    @GET(ROUT)
    fun getTrainPrograms(request: GetTrainProgramsRequest): Single<Response<GetTrainProgramsResponse>>

    @GET(ROUT)
    fun getTrainProgram(request: GetTrainProgramRequest): Single<Response<GetTrainProgramResponse>>

    @POST(ROUT)
    fun createTrainProgram(request: CreateTrainProgramRequest): Single<Response<CreateTrainProgramResponse>>
}