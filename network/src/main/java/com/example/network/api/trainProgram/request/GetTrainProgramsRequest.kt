package com.example.network.api.trainProgram.request

import com.example.network.service.request.BaseQueryRequest
import com.google.gson.annotations.SerializedName

data class GetTrainProgramsRequest(
    @SerializedName("page_index")
    var pageIndex: Int,
    @SerializedName("page_size")
    var pageSize: Int
) : BaseQueryRequest {
    override fun buildParams(): Map<String, String> {
        return mapOf(
            "page_index" to pageIndex.toString(),
            "page_size" to pageSize.toString()
        )
    }
}