package com.example.network.api.user.request

import com.google.gson.annotations.SerializedName

data class GetUserDetailsRequest(
    @SerializedName("user_id")
    var userid: Long
)