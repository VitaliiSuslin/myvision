package com.example.network.api.user.request

import com.google.gson.annotations.SerializedName

data class GetUsersRequest(
    @SerializedName("page_index")
    var pageIndex: Long,
    @SerializedName("page_size")
    var pageSize: Int
)