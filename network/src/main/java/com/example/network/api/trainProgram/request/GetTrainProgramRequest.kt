package com.example.network.api.trainProgram.request

import com.example.network.service.request.BaseQueryRequest
import com.google.gson.annotations.SerializedName

data class GetTrainProgramRequest(
    @SerializedName("train_program_id")
    var trainProgramId: Long
) : BaseQueryRequest {
    override fun buildParams(): Map<String, String> {
        return mapOf(
            "train_program_id" to trainProgramId.toString()
        )
    }

}