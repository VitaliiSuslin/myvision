package com.example.network.api.user.reposne

import com.example.network.api.user.reposne.dto.UserDetailsDto
import com.example.network.service.response.BaseResponse
import com.google.gson.annotations.SerializedName

data class GetUserDetailsResponse(
    @SerializedName("user_details")
    var userDetailsDto: UserDetailsDto
) : BaseResponse() {
    override fun validate(): Boolean {
        return userDetailsDto.validate()
    }

}