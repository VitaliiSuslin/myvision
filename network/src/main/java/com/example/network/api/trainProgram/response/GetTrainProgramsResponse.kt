package com.example.network.api.trainProgram.response

import com.example.network.api.trainProgram.response.dto.TrainProgramDto
import com.example.network.service.response.BaseResponse
import com.google.gson.annotations.SerializedName

data class GetTrainProgramsResponse(
    @SerializedName("train_program")
    var trainPrograms: MutableList<TrainProgramDto>
) : BaseResponse() {
    override fun validate(): Boolean {
        return true
    }

}