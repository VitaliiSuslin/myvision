package com.example.network.api.user.request

import com.google.gson.annotations.SerializedName

data class SignInRequest(
    @SerializedName("email")
    var email: String,
    @SerializedName("password")
    var password: String
)