package com.example.network.service.exceptions

import com.example.network.service.response.*
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

class NetworkException
private constructor(
    val url: String,
    val type: Type,
    val responseCode: String,
    val description: String? = null,
    val originalCause: Throwable? = null
) : RuntimeException(originalCause) {

    enum class Type {
        /**
         * non-200 HTTP status code was received from the server
         */
        HTTP,
        /**
         * IOException occurred while communicating to the server
         */
        NETWORK,
        /**
         * internal error occurred while attempting to execute a request
         */
        UNKNOWN,
        /**
         * server response contains error flag or is invalid
         */
        SERVER
    }

    companion object {

        fun httpError(url: String, throwable: HttpException): NetworkException {
            return NetworkException(
                url,
                Type.HTTP,
                HTTP,
                "${throwable.code()}: ${throwable.message()}",
                throwable
            )
        }

        fun networkError(url: String, throwable: IOException): NetworkException {
            return when (throwable) {
                is NoInternetException -> {
                    NetworkException(
                        url,
                        Type.NETWORK,
                        NO_INTERNET,
                        throwable.message,
                        throwable
                    )
                }
                is SocketTimeoutException -> {
                    NetworkException(
                        url, Type.NETWORK,
                        TIMEOUT, throwable.message, throwable
                    )
                }
                is UnknownHostException -> {
                    NetworkException(
                        url,
                        Type.NETWORK,
                        UNKNOWN_HOST,
                        throwable.message,
                        throwable
                    )
                }
                else -> {
                    NetworkException(
                        url, Type.NETWORK,
                        UNKNOWN, throwable.message, throwable
                    )
                }
            }
        }

        fun unknownError(url: String, throwable: Throwable): NetworkException {
            return NetworkException(
                url, Type.UNKNOWN,
                UNKNOWN, throwable.message, throwable
            )
        }

        fun serverError(url: String, responseCode: String, description: String?): NetworkException {
            return NetworkException(
                url,
                Type.SERVER,
                responseCode,
                description
            )
        }

        fun invalidResponseError(url: String, description: String): NetworkException {
            return NetworkException(
                url, Type.SERVER,
                INVALID_RESPONSE, description
            )
        }
    }

    override val message: String
        get() {
            return """
                |
                |url: $url
                |type: ${type.name}
                |response code: $responseCode
                |description: $description
                |message: ${super.message}
            """.trimMargin()
        }
}