package com.example.network.service.response

import com.example.network.service.exceptions.NetworkException
import io.reactivex.Single
import io.reactivex.functions.Function
import okhttp3.ResponseBody
import retrofit2.HttpException
import retrofit2.Response

class ResponseHandler<R : Response<*>> : Function<R, Single<R>> {

    override fun apply(response: R): Single<R> {
        val requestUrl = response.raw().request.url.toString()

        return if (response.isSuccessful) {
            when (val body = response.body()) {
                is ResponseBody -> Single.just(response)
                is BaseResponse -> {
                    body.validate().let {
                        if (it) {
                            Single.just(response)
                        } else {
                            Single.error(
                                NetworkException.invalidResponseError(
                                    requestUrl,
                                    "${body::class.java.simpleName} is not valid:\n$it"
                                )
                            )
                        }
                    }
                }
                else -> Single.error(ClassCastException("Body should be instance of ResponseBody or BaseResponse"))
            }
        } else {
            Single.error(NetworkException.httpError(requestUrl, HttpException(response)))
        }
    }
}