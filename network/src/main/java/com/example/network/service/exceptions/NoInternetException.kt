package com.example.network.service.exceptions

import java.io.IOException

class NoInternetException : IOException("no internet connection")