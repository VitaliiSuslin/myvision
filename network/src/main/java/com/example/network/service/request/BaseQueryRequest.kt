package com.example.network.service.request

@FunctionalInterface
interface BaseQueryRequest {
    fun buildParams(): Map<String, String>
}