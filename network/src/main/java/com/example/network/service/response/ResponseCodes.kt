package com.example.network.service.response

const val SUCCESS = "success"
const val ACTION_INCORRECT = "action_incorrect"
const val FORBIDDEN = "forbidden"

const val UNKNOWN = "unknown"
const val NO_INTERNET = "no_internet"
const val TIMEOUT = "timeout"
const val UNKNOWN_HOST = "unknown_host"
const val HTTP = "http"
const val INVALID_RESPONSE = "invalid_response"