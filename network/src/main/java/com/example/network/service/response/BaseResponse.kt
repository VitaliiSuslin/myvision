package com.example.network.service.response

import com.google.gson.annotations.SerializedName

abstract class BaseResponse(
    @SerializedName("code")
    var code: Int? = null
) : SelfValidator