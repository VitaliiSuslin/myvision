package com.example.network.service.callAdapter

import retrofit2.CallAdapter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import java.lang.reflect.Type

class CallAdapterFactory<R, T>(
    private val provideWrapper: (callAdapter: CallAdapter<R, T>) -> CallAdapter<R, T>
) : CallAdapter.Factory() {

    private val factory = RxJava2CallAdapterFactory.create()

    override fun get(
        returnType: Type?,
        annotations: Array<out Annotation>?,
        retrofit: Retrofit?
    ): CallAdapter<R, T>? {
        @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS", "UNCHECKED_CAST")
        return provideWrapper(
            factory.get(returnType, annotations, retrofit) as? CallAdapter<R, T>
                ?: return null
        )
    }
}