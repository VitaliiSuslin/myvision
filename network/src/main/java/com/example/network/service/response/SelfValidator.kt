package com.example.network.service.response

interface SelfValidator {
    fun validate(): Boolean
}