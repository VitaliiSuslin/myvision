package com.example.network.service.callAdapter

import io.reactivex.Single
import retrofit2.Call
import retrofit2.CallAdapter

open class CallAdapterWrapper<R : Any>(private val adapter: CallAdapter<R, Single<R>>) :
    CallAdapter<R, Single<R>> by adapter {

    override fun adapt(call: Call<R>): Single<R> {
        return adapter.adapt(call)
            .onErrorResumeNext { throwable ->
                Single.error(throwable)
            }
    }
}