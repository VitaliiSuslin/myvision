package com.example.database.section.trainProgram.entity

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.example.database.BaseEntity

@Entity(
    foreignKeys = [
        ForeignKey(
            entity = DbTrainProgram::class,
            parentColumns = ["id"],
            childColumns = ["trainProgramId"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE
        )
    ]
)
data class DbExercise(
    var title: String,
    var description: String,
    var trainProgramId: Long,
    @PrimaryKey
    var id: Long
) : BaseEntity()