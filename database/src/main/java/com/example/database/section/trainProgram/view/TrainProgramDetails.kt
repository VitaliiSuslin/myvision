package com.example.database.section.trainProgram.view

data class TrainProgramDetails(
    var title: String,
    var description: String
)