package com.example.database.section.trainProgram.dao

import androidx.room.Dao
import com.example.database.AppDatabase
import com.example.database.BaseDao
import com.example.database.section.trainProgram.entity.DbExercise

@Dao
abstract class ExerciseDao(appDatabase: AppDatabase) : BaseDao<DbExercise>(appDatabase)