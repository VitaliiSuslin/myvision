package com.example.database.section.user.dao

import androidx.room.Dao
import androidx.room.Query
import com.example.database.AppDatabase
import com.example.database.BaseDao
import com.example.database.section.user.enity.DbUser

@Dao
abstract class UserDao(appDatabase: AppDatabase) : BaseDao<DbUser>(appDatabase) {

    @Query("SELECT * FROM DbUser WHERE id =:userId")
    abstract fun select(userId: Long): DbUser

    @Query("SELECT * FROM DbUser LIMIT :limit OFFSET :offset")
    abstract fun select(limit: Long, offset: Int): List<DbUser>

    @Query("SELECT COUNT(id) FROM DbUser")
    abstract fun selectCount(): Long

    @Query("DELETE FROM DbUser")
    abstract fun delete(): Int
}