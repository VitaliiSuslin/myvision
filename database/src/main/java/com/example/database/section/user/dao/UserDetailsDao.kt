package com.example.database.section.user.dao

import androidx.room.Dao
import com.example.database.AppDatabase
import com.example.database.BaseDao
import com.example.database.section.user.enity.DbUserDetails

@Dao
abstract class UserDetailsDao(database: AppDatabase) : BaseDao<DbUserDetails>(database)