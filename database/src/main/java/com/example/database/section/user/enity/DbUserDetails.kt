package com.example.database.section.user.enity

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey
import com.example.database.BaseEntity

@Entity(
    indices = [
        Index(
            value = ["userId"],
            unique = true
        )
    ],
    foreignKeys = [
        ForeignKey(
        entity = DbUser::class,
            parentColumns = ["id"],
            childColumns = ["userId"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE
        )
    ]
)
data class DbUserDetails(
    var userId: Long,
    var dateBirth: Long? = null,
    var weight: Float? = null,
    var height: Float? = null,
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0L
):BaseEntity()