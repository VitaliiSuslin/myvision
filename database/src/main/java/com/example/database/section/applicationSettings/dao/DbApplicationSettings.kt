package com.example.database.section.applicationSettings.dao

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.example.database.BaseEntity

@Entity(
    indices = [
        Index("profileUserId")
    ]
)
data class DbApplicationSettings(
    var profileUserId: Long,
    var jwtToken: String,

    var serverTotalUsers: Long = 0L,
    var serverTotalTrainPrograms: Long = 0L,

    @PrimaryKey(autoGenerate = true)
    var id: Long = 0L
):BaseEntity()