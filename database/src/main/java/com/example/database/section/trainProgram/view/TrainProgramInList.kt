package com.example.database.section.trainProgram.view

data class TrainProgramInList(
    var id:Long,
    var title: String
)