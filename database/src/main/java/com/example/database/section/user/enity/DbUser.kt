package com.example.database.section.user.enity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.database.BaseEntity

@Entity
data class DbUser(
    @PrimaryKey
    var id: Long,
    var userName: String,
    var email: String
) : BaseEntity()