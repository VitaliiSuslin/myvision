package com.example.database.section.trainProgram.dao

import androidx.room.Dao
import androidx.room.Query
import com.example.database.AppDatabase
import com.example.database.BaseDao
import com.example.database.section.trainProgram.entity.DbTrainProgram
import com.example.database.section.trainProgram.view.TrainProgramDetails
import com.example.database.section.trainProgram.view.TrainProgramInList

@Dao
abstract class TrainProgramDao(appDatabase: AppDatabase) : BaseDao<DbTrainProgram>(appDatabase) {

    @Query("SELECT id,title FROM DbTrainProgram LIMIT :limit OFFSET :startFrom")
    abstract fun selectList(startFrom: Long, limit: Long): List<TrainProgramInList>

    @Query("SELECT title, description FROM DbTrainProgram WHERE id = :trainProgramId")
    abstract fun selectDetails(trainProgramId: Long): TrainProgramDetails?
}