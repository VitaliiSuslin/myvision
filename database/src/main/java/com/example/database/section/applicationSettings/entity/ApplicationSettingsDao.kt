package com.example.database.section.applicationSettings.entity

import androidx.room.Dao
import androidx.room.Query
import com.example.database.AppDatabase
import com.example.database.BaseDao
import com.example.database.section.applicationSettings.dao.DbApplicationSettings

@Dao
abstract class ApplicationSettingsDao(
    database: AppDatabase
) : BaseDao<DbApplicationSettings>(database) {

    @Query("SELECT * FROM DbApplicationSettings WHERE profileUserId = :profileUserId")
    abstract fun select(profileUserId: Long): DbApplicationSettings?
}