package com.example.database.section.trainProgram.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.database.BaseEntity

@Entity
data class DbTrainProgram(
    var title: String,
    var description: String,
    @PrimaryKey
    var id: Long
) : BaseEntity()