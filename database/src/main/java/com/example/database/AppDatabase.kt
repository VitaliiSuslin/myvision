package com.example.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.database.section.applicationSettings.dao.DbApplicationSettings
import com.example.database.section.applicationSettings.entity.ApplicationSettingsDao
import com.example.database.section.trainProgram.dao.ExerciseDao
import com.example.database.section.trainProgram.dao.TrainProgramDao
import com.example.database.section.trainProgram.entity.DbExercise
import com.example.database.section.trainProgram.entity.DbTrainProgram
import com.example.database.section.user.dao.UserDao
import com.example.database.section.user.dao.UserDetailsDao
import com.example.database.section.user.enity.DbUser
import com.example.database.section.user.enity.DbUserDetails
import io.reactivex.Flowable
import io.reactivex.processors.PublishProcessor
import kotlin.reflect.KClass

@Database(
    entities = [
        //region APPLICATION_SETTINGS
        DbApplicationSettings::class,
        //endregion
        //region USER
        DbUser::class,
        DbUserDetails::class,
        //endregion
        //region TRAIN_PROGRAM
        DbTrainProgram::class,
        DbExercise::class
        //endregion
    ],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {

    companion object {
        private const val DATABASE_NAME = "database"

        fun buildDatabase(context: Context): AppDatabase {
            return Room
                .databaseBuilder(context, AppDatabase::class.java, DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .build()
        }
    }

    private val changesProcessor = PublishProcessor.create<Set<KClass<out BaseEntity>>>()

    fun observeChanges(): Flowable<Set<KClass<out BaseEntity>>> = changesProcessor

    fun publishChange(entities: Set<KClass<out BaseEntity>>) {
        changesProcessor.onNext(entities)
    }

    //region APPLICATION_SETTINGS
    abstract fun applicationSettingsDao(): ApplicationSettingsDao
    //endregion

    //region USER
    abstract fun userDao(): UserDao

    abstract fun userDetailsDao(): UserDetailsDao
    //endregion

    //region TRAIN_PROGRAM
    abstract fun trainProgramDao(): TrainProgramDao

    abstract fun exerciseDao(): ExerciseDao
    //endregion
}