package com.example.myvision.dependencies

import android.annotation.SuppressLint
import android.app.Application

@SuppressLint("StaticFieldLeak")
object Injector {

    lateinit var repositories: Repositories
        private set

    lateinit var managers: Managers
        private set

    fun init(application: Application) {
        repositories = Repositories(application)
        managers = Managers(application)
    }
}