package com.example.myvision.dependencies

import android.content.Context
import com.example.myvision.arcitecture.mediators.DatabaseMediator
import com.example.myvision.arcitecture.mediators.RestClientMediator
import com.example.myvision.ui.host.pages.trainPrograms.repository.TrainProgramsRepository
import com.example.myvision.ui.host.pages.users.repository.UsersRepository
import com.example.myvision.ui.host.pages.users.userDetails.repository.UserDetailsRepository
import com.example.myvision.ui.profile.repository.ApplicationSettingRepository
import com.example.myvision.ui.signIn.repository.SignInRepository
import com.example.myvision.ui.signUp.repository.SignUpRepository

class Repositories(context: Context) {

    private val restClientMediator by lazy {
        RestClientMediator(context)
    }

    private val databaseMediator by lazy {
        DatabaseMediator(context)
    }

    val applicationSettingRepository by lazy {
        ApplicationSettingRepository(
            databaseMediator
        )
    }

    val signUpRepository by lazy {
        SignUpRepository(restClientMediator)
    }

    val signInRepository by lazy {
        SignInRepository(restClientMediator)
    }

    val usersRepository by lazy {
        UsersRepository(
            restClientMediator,
            databaseMediator
        )
    }

    val userDetailsRepository by lazy {
        UserDetailsRepository(
            restClientMediator,
            databaseMediator
        )
    }

    val trainProgramsRepository by lazy {
        TrainProgramsRepository(
            restClientMediator,
            databaseMediator
        )
    }
}