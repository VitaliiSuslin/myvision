package com.example.myvision.dependencies

import android.content.Context
import com.example.myvision.ui.profile.ProfileManager

class Managers(context: Context) {

    val profileManager = ProfileManager(context)
}