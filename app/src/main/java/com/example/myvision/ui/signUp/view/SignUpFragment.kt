package com.example.myvision.ui.signUp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import com.example.myvision.R
import com.example.myvision.arcitecture.expectedTrouble.ExpectedTrouble
import com.example.myvision.arcitecture.util.Event
import com.example.myvision.arcitecture.util.MIN_LENGTH_PASSWORD
import com.example.myvision.arcitecture.util.REGEX_EMAIL
import com.example.myvision.arcitecture.view.BaseFragment
import com.example.myvision.arcitecture.view.ViewManager
import com.example.myvision.ui.signUp.viewModel.SignUpViewModel
import kotlinx.android.synthetic.main.fragment_sign_up.*
import java.util.regex.Pattern

class SignUpFragment : BaseFragment() {

    private val viewModel by lazy {
        createViewModel(SignUpViewModel::class)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_sign_up, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initToolbar()
        setupControls()
        setupViewManager(view)
    }

    private fun initToolbar() {
        toolbar.apply {
            setNavigationOnClickListener {
                navigator?.popBackStack()
            }
        }
    }

    private fun setupControls() {
        signUpButton.setOnClickListener(this::onSignUpClicked)
    }

    private fun setupViewManager(rootView: View) {
        viewManager = ViewManager(rootView).apply {
            setupViewManaging(R.id.progressContainer, null, null)
        }
    }

    private fun onSignUpClicked(view: View) {
        if (!checkValidFields()) return

        fun onLoading() {
            viewManager?.showProgress()
            enablingControls(false)
        }

        fun onSuccess() {
            showToast(R.string.operation_success)
            navigator?.popBackStack()
        }

        fun onFailure(throwable: Throwable) {
            viewManager?.showContent()
            enablingControls(true)
            showSnackBar(ExpectedTrouble.localizeExpectedTrouble(throwable))
        }

        viewModel.createUser(
            nameEditText.text.toString(),
            emailEditText.text.toString(),
            passwordEditText.text.toString()
        ).observeResult { event ->
            when (event) {
                is Event.Loading -> onLoading()
                is Event.Success -> onSuccess()
                is Event.Failure -> onFailure(event.failure)
            }
        }
    }

    private fun checkValidFields(): Boolean {
        var isValid = true

        nameEditText.text?.let {
            if (it.isBlank()) {
                isValid = false
                nameErrorTextView.apply {
                    isVisible = true
                    text = getString(R.string.error_empty_field)
                }
                return@let
            }

            if (it.length <= MIN_LENGTH_PASSWORD) {
                isValid = false
                nameErrorTextView.apply {
                    isVisible = true
                    text = getString(R.string.error_name_low_characters)
                }
                return@let
            }

            nameErrorTextView.isVisible = false
        }

        emailEditText.text?.let {
            if (it.isBlank()) {
                isValid = false
                emailErrorTextView.apply {
                    isVisible = true
                    text = getString(R.string.error_empty_field)
                }
                return@let
            }

            if (!Pattern.matches(REGEX_EMAIL, it)) {
                isValid = false
                emailErrorTextView.apply {
                    isVisible = true
                    text = getString(R.string.error_email_not_valid)
                }
                return@let
            }

            emailErrorTextView.isVisible = false
        }

        val password = passwordEditText.text?.let {
            if (it.isBlank()) {
                isValid = false
                passwordErrorTextView.apply {
                    isVisible = true
                    text = getString(R.string.error_empty_field)
                }
                return@let
            }

            if (it.length <= MIN_LENGTH_PASSWORD) {
                isValid = false
                passwordErrorTextView.apply {
                    isVisible = true
                    text = getString(R.string.error_password_low_characters)
                }
                return@let
            }

            passwordErrorTextView.isVisible = false
        }

        confirmPasswordEditText.text?.let {
            if (it.isBlank()) {
                isValid = false
                confirmPasswordErrorTextView.apply {
                    isVisible = true
                    text = getString(R.string.error_empty_field)
                }
                return@let
            }

            if (it.length <= MIN_LENGTH_PASSWORD) {
                isValid = false
                confirmPasswordErrorTextView.apply {
                    isVisible = true
                    text = getString(R.string.error_password_low_characters)
                }
                return@let
            }

            if (it.toString() != password.toString()) {
                isValid = false
                confirmPasswordErrorTextView.apply {
                    isVisible = true
                    text = getString(R.string.error_password_not_confirmed_same)
                }
            }

            passwordErrorTextView.isVisible = false
        }

        return isValid
    }

    private fun enablingControls(enable: Boolean) {
        nameEditText.isEnabled = enable
        emailEditText.isEnabled = enable
        passwordEditText.isEnabled = enable
        signUpButton.isEnabled = enable
        toolbar.isEnabled = enable
    }
}