package com.example.myvision.ui.host.pages.users.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import com.example.database.section.user.enity.DbUser
import com.example.myvision.R
import com.example.myvision.arcitecture.adapter.BaseAdapterViewHolder
import com.example.myvision.arcitecture.adapter.BaseListAdapter
import kotlinx.android.synthetic.main.item_user.view.*

class UsersAdapter
    : BaseListAdapter<DbUser, UsersAdapter.UserViewHolder>(diffUtil) {

    companion object {
        private val diffUtil = object : DiffUtil.ItemCallback<DbUser>() {
            override fun areItemsTheSame(
                oldItem: DbUser,
                newItem: DbUser
            ): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(
                oldItem: DbUser,
                newItem: DbUser
            ): Boolean {
                return oldItem == newItem
            }
        }
    }

    interface ActionListener {
        fun onClicked(user: DbUser)
    }

    var actionListener: ActionListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        return UserViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_user, parent)
        )
    }

    inner class UserViewHolder(itemView: View) : BaseAdapterViewHolder<DbUser>(itemView),
        View.OnClickListener {

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            actionListener?.onClicked(item!!)
        }

        override fun bindView(item: DbUser) {
            itemView.apply {
                userNameTextView.text = item.userName
                emailTextView.text = item.email
            }
        }
    }
}