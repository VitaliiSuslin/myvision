package com.example.myvision.ui.host.pages.users.userDetails.repository

import com.example.myvision.arcitecture.mediators.DatabaseMediator
import com.example.myvision.arcitecture.mediators.RestClientMediator

class UserDetailsRepository(
    private val restClientMediator: RestClientMediator,
    private val databaseMediator: DatabaseMediator
)