package com.example.myvision.ui.host.pages.users.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.example.database.section.user.enity.DbUser
import com.example.myvision.dependencies.Injector
import com.example.myvision.arcitecture.util.Event
import com.example.myvision.arcitecture.util.PAGE_SIZE
import com.example.myvision.arcitecture.util.ioScheduler
import com.example.myvision.arcitecture.util.mainThread
import com.example.myvision.arcitecture.viewModel.BaseViewModel
import com.example.myvision.ui.host.pages.users.datasource.UsersDataSource
import com.example.myvision.ui.host.pages.users.repository.UsersRepository

class UsersViewModel2(
    private val usersRepository: UsersRepository = Injector.repositories.usersRepository
) : BaseViewModel() {

    private val usersDataSource by lazy { UsersDataSource(this@UsersViewModel2, usersRepository) }

    private val usersPageList by lazy {
        LivePagedListBuilder(
            object : DataSource.Factory<Long, DbUser>() {
                override fun create(): DataSource<Long, DbUser> {
                    return usersDataSource
                }
            },
            PagedList.Config.Builder()
                .setPageSize(PAGE_SIZE)
                .setEnablePlaceholders(false)
                .build()
        )
            .build()
    }

    fun usersData(): LiveData<PagedList<DbUser>> {
        return usersPageList
    }

    fun observeState(): LiveData<Event<Any>> {
        return MutableLiveData<Event<Any>>().also { result ->
            usersDataSource.itemsDataSources
                .subscribeOn(ioScheduler)
                .observeOn(mainThread)
                .subscribe {
                    result.value = it
                }.also {
                    addDisposable(it)
                }
        }
    }

    private fun observeUsers(): LiveData<Event<PagedList<DbUser>>> {
        return MutableLiveData<Event<PagedList<DbUser>>>().also { result ->
            usersDataSource.itemsDataSources
                .subscribeOn(ioScheduler)
                .observeOn(mainThread)
                .subscribe {

                    val list = usersPageList.value
                    when (it) {
                        is Event.Loading -> result.value = Event.Loading(list)
//                        is Event.Success -> result.value = Event.Success(list)
                        is Event.Failure -> result.value = Event.Failure(it.failure, list)
                    }
                }.also {
                    addDisposable(it)
                }
        }
    }
}