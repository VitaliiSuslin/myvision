package com.example.myvision.ui.host.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import com.example.myvision.R
import com.example.myvision.arcitecture.view.BaseFragment
import com.example.myvision.ui.host.pages.dashboard.view.DashboardFragment
import com.example.myvision.ui.host.pages.trainPrograms.view.TrainProgramsFragment
import com.example.myvision.ui.host.pages.users.view.UsersFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.fragment_host.*

class HostFragment
    : BaseFragment(),
    BottomNavigationView.OnNavigationItemSelectedListener {

    private val pages by lazy {
        mapOf(
            R.id.dashboard to DashboardFragment.newInstance(),
            R.id.users to UsersFragment.newInstance(),
            R.id.trainPrograms to TrainProgramsFragment.newInstance()
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_host, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupToolbar()
        setupBottomNavigation()
    }

    private fun setupToolbar() {

    }

    private fun setupBottomNavigation() {
        bottomNavigator.setOnNavigationItemSelectedListener(this)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val fragment = pages[item.itemId]
        if (fragment != null) {
            fragmentManager?.beginTransaction()?.replace(R.id.pageContainer, fragment)?.commit()
            return true
        }
        return false
    }
}