package com.example.myvision.ui.splash.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.myvision.R
import com.example.myvision.arcitecture.util.Event
import com.example.myvision.arcitecture.view.BaseFragment
import com.example.myvision.ui.splash.viewModel.SplashViewModel

class SplashFragment : BaseFragment() {

    private val viewModel by lazy {
        createViewModel(SplashViewModel::class)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_splash, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViewManager()
    }

    private fun setupViewManager() {
        viewManager?.apply {
            setupViewManaging(R.id.progressBar, null, null)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        showPreload()
    }

    private fun showPreload() {
        viewModel.preloadInfoResult().observeResult { event ->
            when (event) {
                is Event.Loading -> viewManager?.showProgress()
                is Event.Success -> navigator?.navigate(SplashFragmentDirections.toSignInFragment())
                is Event.Failure -> navigator?.navigate(SplashFragmentDirections.toSignInFragment())
            }
        }
    }

    override fun onDestroy() {
        viewModel.dispose()
        super.onDestroy()
    }
}