package com.example.myvision.ui.signUp.repository

import com.example.myvision.arcitecture.mediators.RestClientMediator
import com.example.network.api.user.request.CreateUserRequest
import io.reactivex.Completable
import io.reactivex.Single

class SignUpRepository(
    private val restClientMediator: RestClientMediator
) {
    fun createUser(userName: String, email: String, password: String): Completable {
        return Single.fromCallable {
            CreateUserRequest(userName, email, password)
        }
            .flatMap { request ->
                restClientMediator.call { createUser(request) }
            }
            .ignoreElement()
    }
}