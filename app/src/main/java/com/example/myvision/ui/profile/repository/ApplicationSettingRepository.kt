package com.example.myvision.ui.profile.repository

import com.example.database.section.applicationSettings.dao.DbApplicationSettings
import com.example.myvision.arcitecture.mediators.DatabaseMediator
import com.example.myvision.arcitecture.util.Optional
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

class ApplicationSettingRepository(
    private val databaseMediator: DatabaseMediator
) {
    fun select(profileUserId: Long): Single<Optional<DbApplicationSettings>> {
        return databaseMediator.select {
            Single.fromCallable {
                Optional(applicationSettingsDao().select(profileUserId))
            }
        }
    }

    fun save(appSettings: DbApplicationSettings): Completable {
        return databaseMediator.modify(setOf(DbApplicationSettings::class)) {
            Completable.fromCallable {
                applicationSettingsDao().insert(appSettings)
            }
        }
    }

    fun observe(profileUserId: Long): Flowable<DbApplicationSettings?> {
        return databaseMediator.observe(setOf(DbApplicationSettings::class)) {
            Flowable.fromCallable {
                applicationSettingsDao().select(profileUserId)
            }
        }
    }
}