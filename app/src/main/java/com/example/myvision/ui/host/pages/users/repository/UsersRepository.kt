package com.example.myvision.ui.host.pages.users.repository

import com.example.database.section.applicationSettings.dao.DbApplicationSettings
import com.example.database.section.user.enity.DbUser
import com.example.myvision.arcitecture.mediators.DatabaseMediator
import com.example.myvision.arcitecture.mediators.RestClientMediator
import com.example.myvision.ui.host.pages.users.converter.toUsersWithTotals
import com.example.network.api.user.request.GetUsersRequest
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

class UsersRepository(
    private val restClientMediator: RestClientMediator,
    private val databaseMediator: DatabaseMediator
) {

    fun loadUsers(
        pageIndex: Long,
        pageSize: Int
    ): Single<Pair<List<DbUser>, Long>> {
        return Single.fromCallable {
            GetUsersRequest(pageIndex, pageSize)
        }.flatMap { request ->
            restClientMediator.call { getUsers(request) }
        }.map { response ->
            response.body()!!.toUsersWithTotals()
        }
    }

    fun saveUsersInLocalDb(
        profileUserId: DbApplicationSettings,
        users: List<DbUser>
    ): Completable {
        return databaseMediator.modify(setOf(DbUser::class, DbApplicationSettings::class)) {
            Single.fromCallable {
                runInTransaction {
                    applicationSettingsDao().insert(profileUserId)
                    userDao().insert(users)
                }
            }.ignoreElement()
        }
    }

    fun observeUsers(
        pageIndex: Long,
        pageSize: Int
    ): Flowable<List<DbUser>> {
        return databaseMediator.observe(setOf(DbUser::class)) {
            Flowable.fromCallable {
                userDao().select(pageIndex, pageSize)
            }
        }
    }

    fun selectUsersCountInLocalDb(): Single<Long> {
        return databaseMediator.select {
            Single.fromCallable {
                userDao().selectCount()
            }
        }
    }

    fun selectUsers(
        pageIndex: Long,
        pageSize: Int
    ): Single<List<DbUser>> {
        return databaseMediator.select {
            Single.fromCallable {
                userDao().select(pageIndex, pageSize)
            }
        }
    }
}