package com.example.myvision.ui.signUp.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.myvision.dependencies.Injector
import com.example.myvision.arcitecture.util.Event
import com.example.myvision.arcitecture.util.ioScheduler
import com.example.myvision.arcitecture.util.mainThread
import com.example.myvision.arcitecture.viewModel.BaseViewModel
import com.example.myvision.ui.signUp.repository.SignUpRepository
import timber.log.Timber

class SignUpViewModel(
    private val signUpRepository: SignUpRepository = Injector.repositories.signUpRepository
) : BaseViewModel() {

    fun createUser(userName: String, email: String, password: String): LiveData<Event<Any>> {
        return MutableLiveData<Event<Any>>().also { result ->
            signUpRepository.createUser(userName, email, password)
                .subscribeOn(ioScheduler)
                .observeOn(mainThread)
                .doOnSubscribe {
                    result.value = Event.Loading()
                }
                .subscribe({
                    result.value = Event.Success(Any())
                }, {
                    Timber.e(it)
                    result.value = Event.Failure(it)
                })
        }
    }
}