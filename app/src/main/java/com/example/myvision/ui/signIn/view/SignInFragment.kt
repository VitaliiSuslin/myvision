package com.example.myvision.ui.signIn.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import com.example.myvision.R
import com.example.myvision.arcitecture.expectedTrouble.ExpectedTrouble
import com.example.myvision.arcitecture.util.Event
import com.example.myvision.arcitecture.util.MIN_LENGTH_PASSWORD
import com.example.myvision.arcitecture.util.REGEX_EMAIL
import com.example.myvision.arcitecture.view.BaseFragment
import com.example.myvision.arcitecture.view.ViewManager
import com.example.myvision.ui.signIn.viewModel.SignInViewModel
import kotlinx.android.synthetic.main.fragment_sign_in.*
import java.util.regex.Pattern

class SignInFragment : BaseFragment() {

    private val viewModel by lazy {
        createViewModel(SignInViewModel::class)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_sign_in, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupControls()
    }

    private fun setupControls() {
        signInButton.setOnClickListener(this::onSignInClicked)
        signUpButton.setOnClickListener(this::onSignUpClicked)
    }

    private fun setupViewManager(rootView: View) {
        viewManager = ViewManager(rootView).apply {
            setupViewManaging(R.id.progressContainer, null, null)
        }
    }

    private fun onSignUpClicked(view: View) {
        navigator?.navigate(SignInFragmentDirections.toSignUpFragment())
    }

    private fun onSignInClicked(view: View) {
        if (!checkValidFields()) return

        fun onLoading() {
            enableControls(false)
            viewManager?.showProgress()
        }

        fun onSuccess() {
            navigator?.navigate(SignInFragmentDirections.toHostFragment())
        }

        fun onFailure(throwable: Throwable) {
            enableControls(true)
            viewManager?.showContent()
            showSnackBar(ExpectedTrouble.localizeExpectedTrouble(throwable))
        }


        viewModel.signInUser(
            emailEditText.text.toString(),
            passwordEditText.text.toString()
        ).observeResult { event ->
            when (event) {
                is Event.Loading -> onLoading()
                is Event.Success -> onSuccess()
                is Event.Failure -> onFailure(event.failure)
            }
        }
    }

    private fun checkValidFields(): Boolean {
        var isValid = true

        emailEditText.text?.let {
            if (it.isBlank()) {
                isValid = false
                emailErrorTextView.apply {
                    isVisible = true
                    text = getString(R.string.error_empty_field)
                }
                return@let
            }

            if (!Pattern.matches(REGEX_EMAIL, it)) {
                isValid = false
                emailErrorTextView.apply {
                    isVisible = true
                    text = getString(R.string.error_email_not_valid)
                }
                return@let
            }

            emailErrorTextView.isVisible = false
        }

        passwordEditText.text?.let {
            if (it.isBlank()) {
                isValid = false
                passwordErrorTextView.apply {
                    isVisible = true
                    text = getString(R.string.error_empty_field)
                }
                return@let
            }

            if (it.length <= MIN_LENGTH_PASSWORD) {
                isValid = false
                passwordErrorTextView.apply {
                    isVisible = true
                    text = getString(R.string.error_password_low_characters)
                }
                return@let
            }

            passwordErrorTextView.isVisible = false
        }

        return isValid
    }

    private fun enableControls(enable: Boolean) {
        signInButton.isEnabled = enable
        signUpButton.isEnabled = enable
        emailEditText.isEnabled = enable
        passwordEditText.isEnabled = enable
    }
}