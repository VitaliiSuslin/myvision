package com.example.myvision.ui.host.pages.trainPrograms.repository

import com.example.myvision.arcitecture.mediators.DatabaseMediator
import com.example.myvision.arcitecture.mediators.RestClientMediator

class TrainProgramsRepository(
    var restClientMediator: RestClientMediator,
    var databaseMediator: DatabaseMediator
)