package com.example.myvision.ui.host.pages.users.converter

import com.example.database.section.user.enity.DbUser
import com.example.network.api.user.reposne.GetUsersResponse
import com.example.network.api.user.reposne.dto.UserDto

fun GetUsersResponse.toUsersWithTotals(): Pair<List<DbUser>, Long> {
    return Pair(users.map { it.toDbUser() }, totalSize)
}

fun UserDto.toDbUser(): DbUser {
    return DbUser(userId, userName, email)
}