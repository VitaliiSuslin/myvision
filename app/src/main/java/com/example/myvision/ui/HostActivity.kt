package com.example.myvision.ui

import android.content.pm.PackageManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.example.myvision.R

class HostActivity : AppCompatActivity() {

    companion object {
        const val PERMISSION_REQUEST_CODE = 1240
    }

    interface ActionListener {
        fun onRequestPermissionsResult(isApprove: Boolean)
    }

    private var actionListener: ActionListener? = null
    var navigationController: NavController? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_host)
        navigationController = Navigation.findNavController(
            this,
            R.id.navigation_host_fragment
        )
    }

    /**
     * Checking permission
     * */
    fun checkPermission(
        actionListener: ActionListener,
        permissions: Array<String>
    ) {
        this.actionListener = actionListener
        var isApprove = true
        permissions.forEach {
            isApprove =
                ContextCompat.checkSelfPermission(this, it) == PackageManager.PERMISSION_GRANTED
        }

        if (!isApprove) {
            ActivityCompat.requestPermissions(this, permissions, PERMISSION_REQUEST_CODE)
        }

        actionListener.onRequestPermissionsResult(isApprove)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    actionListener?.onRequestPermissionsResult(true)
                } else {
                    actionListener?.onRequestPermissionsResult(false)
                }
            }
        }
    }
}
