package com.example.myvision.ui.host.pages.trainPrograms.trainProgramDetails.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.myvision.R
import com.example.myvision.arcitecture.view.BaseFragment
import com.example.myvision.arcitecture.view.ViewManager
import com.example.myvision.ui.host.pages.trainPrograms.trainProgramDetails.viewModel.TrainProgramViewModel
import kotlinx.android.synthetic.main.fragment_train_program_details.*

class TrainProgramDetailsFragment : BaseFragment() {

    private val viewModel by lazy {
        createViewModel(TrainProgramViewModel::class)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_train_program_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initToolbar()
        initViewManager(view)
    }

    private fun initToolbar() {
        toolBar?.apply {
            setNavigationOnClickListener {
                navigator?.popBackStack()
            }
        }
    }

    private fun initViewManager(rootView: View) {
        viewManager = ViewManager(rootView).apply {
            setupViewManaging(R.id.progressBar, R.id.recyclerView, R.id.placeholder)
        }
    }
}