package com.example.myvision.ui.profile

import android.content.Context
import com.example.database.section.applicationSettings.dao.DbApplicationSettings
import com.example.myvision.dependencies.Injector
import com.example.myvision.arcitecture.expectedTrouble.ExpectedTrouble
import io.reactivex.Single

/**
 * @author Vitalii Suslin 18.10.2019
 *
 * [ProfileManager] contains function to manage application settings for profile.
 * Use it for authorization requests by getting token, manege application preferences
 * */
class ProfileManager(context: Context) {

    var profileUserId = 1L

    fun getApplicationSettings(): Single<DbApplicationSettings> {
        return Injector.repositories.applicationSettingRepository.select(profileUserId)
            .flatMap { (appSettings) ->
                if (appSettings != null) {
                    Single.just(appSettings)
                } else {
                    Single.error(ExpectedTrouble(ExpectedTrouble.ExpectedCode.FAILED_TO_LOAD_DATA))
                }
            }
    }
}