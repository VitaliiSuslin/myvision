package com.example.myvision.ui.host.pages.users.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.database.section.user.enity.DbUser
import com.example.myvision.arcitecture.util.Event
import com.example.myvision.arcitecture.util.PAGE_SIZE
import com.example.myvision.arcitecture.util.ioScheduler
import com.example.myvision.arcitecture.util.mainThread
import com.example.myvision.arcitecture.viewModel.BaseViewModel
import com.example.myvision.dependencies.Injector
import com.example.myvision.ui.host.pages.users.repository.UsersRepository
import com.example.myvision.ui.profile.ProfileManager
import io.reactivex.Completable
import io.reactivex.disposables.Disposable
import timber.log.Timber

class UsersViewModel(
    private val profileManager: ProfileManager = Injector.managers.profileManager,
    private val usersRepository: UsersRepository = Injector.repositories.usersRepository
) : BaseViewModel() {

    var pageIndex = 1L
    private val usersData by lazy { MutableLiveData<Event<List<DbUser>>>() }

    fun usersData(): LiveData<Event<List<DbUser>>> {
        return usersData.also {
            addDisposable(observerUsers())
        }
    }

    private fun observerUsers(): Disposable {
        return usersRepository.observeUsers(pageIndex, PAGE_SIZE)
            .subscribeOn(ioScheduler)
            .observeOn(mainThread)
            .doOnSubscribe {
                usersData.value = Event.Loading(usersData.value?.arguments)
            }
            .subscribe({
                usersData.value = Event.Success(it)
            }, {
                Timber.e(it)
                usersData.value = Event.Failure(it, usersData.value?.arguments)
            })
    }

    fun loadingUsers(): LiveData<Event<Any>> {
        return MutableLiveData<Event<Any>>().also { result ->
            profileManager.getApplicationSettings()
                .flatMap { appSettings ->
                    usersRepository.selectUsersCountInLocalDb()
                        .map { localCount ->
                            Pair(appSettings, localCount)
                        }
                }
                .flatMapCompletable { (appSettings, localCount) ->
                    if (appSettings.serverTotalUsers != localCount) {
                        usersRepository.loadUsers(pageIndex, PAGE_SIZE)
                            .flatMapCompletable { (users, total) ->
                                appSettings.serverTotalUsers = total
                                usersRepository.saveUsersInLocalDb(appSettings, users)
                            }
                    } else {
                        Completable.complete()
                    }
                }
                .subscribeOn(ioScheduler)
                .observeOn(mainThread)
                .doOnSubscribe {
                    result.value = Event.Loading()
                }
                .subscribe({
                    result.value = Event.Success(Any())
                }, {
                    Timber.e(it)
                    result.value = Event.Failure(it)
                })
        }
    }
}