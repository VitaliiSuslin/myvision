package com.example.myvision.ui.customView

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.myvision.R

class CustomPlaceholder(context: Context) : ConstraintLayout(context) {

    private var mView: View? = null

    init {
        mView = LayoutInflater.from(context).inflate(R.layout.custom_placeholder, this, false)
        requestLayout()
    }

    fun setText(@StringRes resId: Int) {
        mView?.findViewById<TextView>(R.id.placeHolderTextView)?.setText(resId)
        requestLayout()
    }

    fun setImage(@DrawableRes drawableResId: Int) {
        mView?.findViewById<ImageView>(R.id.placeholderImage)?.setImageResource(drawableResId)
        requestLayout()
    }

}