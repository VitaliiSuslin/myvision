package com.example.myvision.ui.host.pages.trainPrograms.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import com.example.database.section.trainProgram.view.TrainProgramInList
import com.example.myvision.R
import com.example.myvision.arcitecture.adapter.BaseAdapterViewHolder
import com.example.myvision.arcitecture.adapter.BaseListAdapter
import kotlinx.android.synthetic.main.item_train_program.view.*

class TrainProgramsAdapter :
    BaseListAdapter<TrainProgramInList, TrainProgramsAdapter.TrainProgramViewHolder>(diffUtil) {

    companion object {
        private val diffUtil = object : DiffUtil.ItemCallback<TrainProgramInList>() {
            override fun areItemsTheSame(
                oldItem: TrainProgramInList,
                newItem: TrainProgramInList
            ): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(
                oldItem: TrainProgramInList,
                newItem: TrainProgramInList
            ): Boolean {
                return oldItem == newItem
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TrainProgramViewHolder {
        return TrainProgramViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_train_program, parent)
        )
    }

    inner class TrainProgramViewHolder(itemView: View) :
        BaseAdapterViewHolder<TrainProgramInList>(itemView) {
        override fun bindView(item: TrainProgramInList) {
            itemView.apply {
                titleTextView.text = item.title
            }
        }
    }
}