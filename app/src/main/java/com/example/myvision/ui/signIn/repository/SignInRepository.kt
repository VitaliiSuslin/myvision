package com.example.myvision.ui.signIn.repository

import com.example.myvision.arcitecture.mediators.RestClientMediator
import com.example.network.api.user.request.SignInRequest
import io.reactivex.Single

class SignInRepository(
    private val restClientMediator: RestClientMediator
) {
    fun signInUser(email: String, password: String): Single<String> {
        return Single.fromCallable {
            SignInRequest(email, password)
        }
            .flatMap { request ->
                restClientMediator.call { signInUser(request) }
            }
            .map { response ->
                response.body()!!.token
            }
    }
}