package com.example.myvision.ui.signIn.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.myvision.dependencies.Injector
import com.example.myvision.arcitecture.util.Event
import com.example.myvision.arcitecture.util.ioScheduler
import com.example.myvision.arcitecture.util.mainThread
import com.example.myvision.arcitecture.viewModel.BaseViewModel
import com.example.myvision.ui.signIn.repository.SignInRepository
import timber.log.Timber

class SignInViewModel(
    private val signInRepository: SignInRepository = Injector.repositories.signInRepository
) : BaseViewModel() {

    fun signInUser(email: String, password: String): LiveData<Event<Any>> {
        return MutableLiveData<Event<Any>>().also { result ->
            signInRepository.signInUser(email, password)
                .subscribeOn(ioScheduler)
                .observeOn(mainThread)
                .doOnSubscribe {
                    result.value = Event.Loading()
                }
                .subscribe({
                    result.value = Event.Success(it)
                }, {
                    Timber.e(it)
                    result.value = Event.Failure(it)
                })
        }
    }
}