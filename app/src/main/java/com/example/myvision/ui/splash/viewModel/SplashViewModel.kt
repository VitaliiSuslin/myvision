package com.example.myvision.ui.splash.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.myvision.arcitecture.util.Event
import com.example.myvision.arcitecture.util.ioScheduler
import com.example.myvision.arcitecture.util.mainThread
import com.example.myvision.arcitecture.util.timeFrizzing
import com.example.myvision.arcitecture.viewModel.BaseViewModel
import io.reactivex.Observable
import timber.log.Timber
import java.util.concurrent.TimeUnit

class SplashViewModel : BaseViewModel() {

    fun preloadInfoResult(): LiveData<Event<Any>> {
        return MutableLiveData<Event<Any>>().also { result ->
            Observable.timer(timeFrizzing, TimeUnit.MILLISECONDS)
                .subscribeOn(ioScheduler)
                .observeOn(mainThread)
                .doOnSubscribe {
                    result.value = Event.Loading()
                }
                .subscribe({
                    result.value = Event.Success(Any())
                }, {
                    Timber.e(it)
                    result.value = Event.Failure(it)
                }).also {
                    addDisposable(it)
                }
        }
    }
}