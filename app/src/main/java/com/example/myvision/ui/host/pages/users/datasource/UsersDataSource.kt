package com.example.myvision.ui.host.pages.users.datasource

import androidx.paging.PageKeyedDataSource
import com.example.database.section.user.enity.DbUser
import com.example.myvision.arcitecture.util.Event
import com.example.myvision.arcitecture.util.ioScheduler
import com.example.myvision.arcitecture.util.mainThread
import com.example.myvision.ui.host.pages.users.repository.UsersRepository
import com.example.myvision.ui.host.pages.users.viewModel.UsersViewModel2
import io.reactivex.processors.PublishProcessor
import timber.log.Timber

class UsersDataSource(
    private val usersViewModel: UsersViewModel2,
    private val usersRepository: UsersRepository
) : PageKeyedDataSource<Long, DbUser>() {

    private val initialPage = 1L
    private var currentPage = 1L
    val itemsDataSources by lazy { PublishProcessor.create<Event<Any>>() }

    override fun loadInitial(
        params: LoadInitialParams<Long>,
        callback: LoadInitialCallback<Long, DbUser>
    ) {
        usersViewModel.addDisposable(
            usersRepository.selectUsers(initialPage, params.requestedLoadSize)
                .subscribeOn(ioScheduler)
                .observeOn(mainThread)
                .doOnSubscribe {
                    itemsDataSources.onNext(Event.Loading())
                }
                .subscribe({
                    itemsDataSources.onNext(Event.Success(Any()))
                    callback.onResult(it, null, currentPage++)
                }, {
                    Timber.e(it)
                    itemsDataSources.onNext(Event.Failure(it))
                })
        )

    }

    override fun loadAfter(params: LoadParams<Long>, callback: LoadCallback<Long, DbUser>) {
        usersViewModel.addDisposable(
            usersRepository.selectUsers(
                params.key,
                params.requestedLoadSize
            )
                .subscribeOn(ioScheduler)
                .observeOn(mainThread)
                .doOnSubscribe {
                    itemsDataSources.onNext(Event.Loading())
                }
                .subscribe({
                    itemsDataSources.onNext(Event.Success(Any()))
                    callback.onResult(it, params.key + 1)
                }, {
                    Timber.e(it)
                    itemsDataSources.onNext(Event.Failure(it))
                })
        )
    }

    override fun loadBefore(params: LoadParams<Long>, callback: LoadCallback<Long, DbUser>) {}
}