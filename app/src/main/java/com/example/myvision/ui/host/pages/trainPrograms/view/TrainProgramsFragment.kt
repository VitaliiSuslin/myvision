package com.example.myvision.ui.host.pages.trainPrograms.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.myvision.R
import com.example.myvision.arcitecture.view.BaseFragment
import com.example.myvision.arcitecture.view.ViewManager
import com.example.myvision.ui.host.pages.trainPrograms.viewModel.TrainProgramsViewModel

class TrainProgramsFragment : BaseFragment() {

    companion object {
        fun newInstance(): TrainProgramsFragment {
            return TrainProgramsFragment()
        }
    }

    private val viewModel by lazy {
        createViewModel(TrainProgramsViewModel::class)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_train_programs, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initToolbar()
        initRecyclerView()
        initViewManager(view)
    }

    private fun initViewManager(rootView: View) {
        viewManager = ViewManager(rootView).apply {
            setupViewManaging(R.id.progressBar, R.id.recyclerView, R.id.placeholder)
        }
    }

    private fun initToolbar() {

    }

    private fun initRecyclerView() {

    }
}