package com.example.myvision.ui.host.pages.users.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.database.section.user.enity.DbUser
import com.example.myvision.R
import com.example.myvision.arcitecture.expectedTrouble.ExpectedTrouble
import com.example.myvision.arcitecture.util.Event
import com.example.myvision.arcitecture.view.BaseListFragment
import com.example.myvision.arcitecture.view.ViewManager
import com.example.myvision.ui.host.pages.users.adapter.UsersAdapter
import com.example.myvision.ui.host.pages.users.viewModel.UsersViewModel
import kotlinx.android.synthetic.main.fragment_users.*

class UsersFragment :
    BaseListFragment(),
    UsersAdapter.ActionListener {

    companion object {
        fun newInstance(): UsersFragment {
            return UsersFragment()
        }
    }

    private val viewModel by lazy {
        createViewModel(UsersViewModel::class)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observerList()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_users, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        setupViewManager(view)
    }

    private fun setupRecyclerView() {
        recyclerView.apply {
            adapter = UsersAdapter().apply {
                actionListener = this@UsersFragment
            }
            addOnScrollListener(onScrollListener)
        }
    }

    private fun setupViewManager(rootView: View) {
        viewManager = ViewManager(rootView).apply {
            setupViewManaging(R.id.progressBar, R.id.recyclerView, R.id.placeholder)
        }
    }

    override fun onClicked(user: DbUser) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private fun observerList() {
        fun onLoading(event: Event.Loading<List<DbUser>>) {
            if (event.data != null) {
                updateList(event.data)
            } else {
                viewManager?.showProgress()
            }
        }

        fun onSuccess(event: Event.Success<List<DbUser>>) {
            updateList(event.data)
        }

        fun onFailure(event: Event.Failure<List<DbUser>>) {
            if (event.data != null) {
                updateList(event.data)
            } else {
                viewManager?.showPlaceholder {
                    placeholder.setText(ExpectedTrouble.localizeExpectedTrouble(event.failure))
                    placeholder.setImage(R.drawable.ic_cave)
                }
            }
        }

        viewModel.usersData().observeData { event ->
            when (event) {
                is Event.Loading -> onLoading(event)
                is Event.Success -> onSuccess(event)
                is Event.Failure -> onFailure(event)
            }
        }
    }

    private fun updateList(users: List<DbUser>) {
        if (users.isNotEmpty()) {
            (recyclerView.adapter as? UsersAdapter)?.submitList(users)
        } else {
            showEmptyPlaceHolder()
        }
    }

    private fun showEmptyPlaceHolder() {
        viewManager?.showPlaceholder {
            placeholder.setText(R.string.no_users)
            placeholder.setImage(R.drawable.ic_empty)
        }
    }

    private fun showLoadProgress() {
        val count = recyclerView.adapter?.itemCount
        if (count == 0) {
            viewManager?.showProgress()
        }
    }

    override fun loadList() {

        fun onLoading() {
            showLoadProgress()
        }

        fun onSuccess() {
            val itemCount = recyclerView.adapter?.itemCount
            if (itemCount != null && itemCount > 0) {
                viewManager?.showContent()
            } else {
                showEmptyPlaceHolder()
            }
        }

        fun onFailure(throwable: Throwable) {
            viewManager?.showPlaceholder {
                placeholder.setText(ExpectedTrouble.localizeExpectedTrouble(throwable))
                placeholder.setImage(R.drawable.ic_cave)
            }
        }

        viewModel.loadingUsers().observeResult { event ->
            when (event) {
                is Event.Loading -> onLoading()
                is Event.Success -> onSuccess()
                is Event.Failure -> onFailure(event.failure)
            }
        }
    }
}