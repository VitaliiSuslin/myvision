package com.example.myvision

import android.app.Application
import com.example.myvision.dependencies.Injector
import timber.log.Timber

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        Injector.init(this)
        initLogging()
    }

    private fun initLogging() {
        if (Timber.forest().isEmpty()) {
            Timber.plant(Timber.DebugTree())
        }
    }
}