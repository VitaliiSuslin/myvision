package com.example.myvision.arcitecture.util

const val REGEX_EMAIL = "\\b[A-Z0-9._%-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}\\b"
const val MIN_LENGTH_PASSWORD = 6
const val MIN_LENGTH_NAME = 3
const val PAGE_SIZE = 20