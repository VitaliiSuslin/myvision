package com.example.myvision.arcitecture.view

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.myvision.arcitecture.util.Event
import com.example.myvision.arcitecture.viewModel.BaseViewModel
import com.example.myvision.ui.HostActivity
import com.google.android.material.snackbar.Snackbar
import kotlin.reflect.KClass

abstract class BaseFragment : Fragment() {
    protected var viewManager: ViewManager? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewManager = ViewManager(view)
    }

    val navigator by lazy {
        (activity as? HostActivity)?.navigationController
    }

    fun <T : BaseViewModel> createViewModel(viewModelClass: KClass<T>): T =
        ViewModelProviders
            .of(this)
            .get(viewModelClass.java)

    fun <T> LiveData<Event<T>>.observeResult(action: (Event<T>) -> Unit) {
        this.observe(this@BaseFragment.viewLifecycleOwner, Observer { event ->
            when (event) {
                is Event.Loading -> action.invoke(event)
                is Event.Success -> action.invoke(event).also { this.removeObserver(action) }
                is Event.Failure -> action.invoke(event).also { this.removeObserver(action) }
            }
        })
    }

    fun <T> LiveData<Event<T>>.observeData(action: (Event<T>) -> Unit) {
        this.observe(this@BaseFragment.viewLifecycleOwner, Observer {
            action.invoke(it)
        })
    }

    protected fun showSnackBar(@StringRes resId: Int, length: Int = Snackbar.LENGTH_LONG) {
        Snackbar.make(view!!, resId, length).show()
    }

    protected fun showToast(@StringRes resId: Int, length: Int = Toast.LENGTH_LONG) {
        Toast.makeText(context!!, resId, length).show()
    }
}