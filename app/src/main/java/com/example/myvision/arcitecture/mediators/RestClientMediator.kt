package com.example.myvision.arcitecture.mediators

import android.content.Context
import com.example.myvision.arcitecture.expectedTrouble.ExpectedTrouble
import com.example.myvision.arcitecture.expectedTrouble.ExpectedTrouble.Companion.toDeterminedError
import com.example.network.RestClient
import com.example.network.service.exceptions.NoInternetException
import io.reactivex.Single
import timber.log.Timber

class RestClientMediator(context: Context) {

    private val restClient = RestClient(context)

    fun <T> call(
        call: RestClient.() -> Single<T>
    ): Single<T> {
        return call(restClient)
            .onErrorResumeNext { throwable ->
                Timber.e(throwable)
                Single.error(
                    toDeterminedError(
                        throwable,
                        if (throwable is NoInternetException)
                            ExpectedTrouble.ExpectedCode.NO_INTERNET
                        else
                            ExpectedTrouble.ExpectedCode.FAILED_TO_LOAD_DATA
                    )
                )
            }
    }
}