package com.example.myvision.arcitecture.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class BaseAdapterViewHolder<T : Any>(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var item: T? = null
    abstract fun bindView(item: T)
}