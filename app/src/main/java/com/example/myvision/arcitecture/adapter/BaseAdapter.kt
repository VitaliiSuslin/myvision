package com.example.myvision.arcitecture.adapter

import androidx.recyclerview.widget.RecyclerView


@Deprecated(message = "Use BaseListAdapter")
abstract class BaseAdapter<T : Any, VH : BaseAdapterViewHolder<T>> :
    RecyclerView.Adapter<VH>() {

    protected var items = hashSetOf<T>()

    override fun getItemCount(): Int {
        return if (items.size >= 0) items.size else 0
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        (holder as? BaseAdapterViewHolder<T>)?.bindView(items.elementAt(position))
    }

    fun addItem(item: T) {
        items.plus(item)
        notifyItemInserted(itemCount)
    }

    fun removeItem(position: Int) {
        val item = items.elementAtOrNull(position)
        if (item != null) {
            items.remove(item)
            notifyItemChanged(position)
        }
    }

    fun addItems(items: HashSet<T>) {
        this.items = items
        notifyDataSetChanged()
    }

    fun updateItem(item: T) {
        items
            .find { it == item }
            ?.let {
                notifyItemChanged(items.indexOf(it))
            }
    }
}