package com.example.myvision.arcitecture.view

import androidx.recyclerview.widget.RecyclerView

abstract class BaseListFragment : BaseFragment() {
    abstract fun loadList()

    protected val onScrollListener by lazy {
        object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy == 0) loadList()
            }
        }
    }
}