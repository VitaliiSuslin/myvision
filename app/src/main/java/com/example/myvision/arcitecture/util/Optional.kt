package com.example.myvision.arcitecture.util

data class Optional<T : Any>(var value: T?)