package com.example.myvision.arcitecture.util

sealed class Event<out T>(
    val arguments: T? = null,
    val throwable: Throwable? = null
) {

    data class Loading<out T>(
        val data: T? = null
    ) : Event<T>(data)

    data class Success<out T>(
        val data: T
    ) : Event<T>(data)

    data class Failure<out T>(
        val failure: Throwable,
        val data: T? = null
    ) : Event<T>(data, failure)
}