package com.example.myvision.arcitecture.util

fun startFrom(pageIndex: Long, pageSize: Int): Long {
    val result = ((pageIndex - 1L) * pageSize) + 1L
    return if (result > 0) result else 0
}

fun processPageIndex(pageIndex: Long, totalCount: Long, localCount: Long): Long {
    return if (totalCount != localCount) pageIndex + 1L else pageIndex
}
