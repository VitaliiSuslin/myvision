package com.example.myvision.arcitecture.viewModel

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseViewModel : ViewModel() {

    private val compositeDisposable by lazy { CompositeDisposable() }

    fun addDisposable(disposable: Disposable) {
        compositeDisposable.apply {
            if (!isDisposed) add(disposable)
        }
    }

    fun resetDisposable(disposable: Disposable) {
        compositeDisposable.apply {
            if (remove(disposable)) add(disposable)
        }
    }

    fun dispose() {
        compositeDisposable.apply {
            if (size() > 0) dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        dispose()
    }
}