package com.example.myvision.arcitecture.util

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

val ioScheduler: Scheduler = Schedulers.io()
val mainThread: Scheduler = AndroidSchedulers.mainThread()

const val timeFrizzing = 5_000L