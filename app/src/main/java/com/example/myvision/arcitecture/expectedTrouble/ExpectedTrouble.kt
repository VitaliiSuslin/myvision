package com.example.myvision.arcitecture.expectedTrouble

import androidx.annotation.StringRes
import com.example.myvision.R

/**
 * [ExpectedTrouble] is class customized Throwable to expect problems and react for them
 * */
class ExpectedTrouble(
    private val expectedCode: ExpectedCode
) : Throwable() {

    companion object {
        @StringRes
        fun localizeExpectedTrouble(throwable: Throwable): Int {
            return when (throwable) {
                is ExpectedTrouble -> getMessageResId(throwable)
                else -> R.string.error_unknown
            }
        }

        private fun getMessageResId(trouble: ExpectedTrouble): Int {
            return when (trouble.expectedCode) {
                ExpectedCode.PERMISSION_FORBIDDEN -> R.string.error_permission_forbidden
                ExpectedCode.NO_INTERNET -> R.string.error_no_internet
                ExpectedCode.FAILED_TO_LOAD_DATA -> R.string.error_failed_to_load_data
                ExpectedCode.FAILED_TO_SAVE_CHANGES -> R.string.error_failed_to_save_data
            }
        }

        fun toDeterminedError(throwable: Throwable, errorCode: ExpectedCode?): Throwable {
            return if (errorCode != null) ExpectedTrouble(errorCode) else throwable
//            return errorCode?.let { ExpectedTrouble(it) } ?: throwable --> working joke code :D
        }
    }

    enum class ExpectedCode {
        PERMISSION_FORBIDDEN,
        FAILED_TO_LOAD_DATA,
        FAILED_TO_SAVE_CHANGES,
        NO_INTERNET
    }
}