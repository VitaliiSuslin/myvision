package com.example.myvision.arcitecture.mediators

import android.content.Context
import androidx.sqlite.db.SimpleSQLiteQuery
import com.example.database.AppDatabase
import com.example.database.BaseEntity
import com.example.myvision.arcitecture.expectedTrouble.ExpectedTrouble
import com.example.myvision.arcitecture.expectedTrouble.ExpectedTrouble.Companion.toDeterminedError
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import kotlin.reflect.KClass

class DatabaseMediator(context: Context) {

    private val appDatabase = AppDatabase.buildDatabase(context)

    fun <T> observe(observer: AppDatabase.() -> Flowable<T>): Flowable<T> {
        return observer.invoke(appDatabase)
            .onErrorResumeNext { error: Throwable ->
                Flowable.error(
                    toDeterminedError(
                        error,
                        ExpectedTrouble.ExpectedCode.FAILED_TO_LOAD_DATA
                    )
                )
            }
    }

    fun <T> observe(
        events: Set<KClass<out BaseEntity>>,
        observer: AppDatabase.() -> Flowable<T>
    ): Flowable<T> {
        return appDatabase.observeChanges()
            .filter { it == events }
            .flatMap {
                observer.invoke(appDatabase)
            }
    }

    fun <T> select(getter: AppDatabase.() -> Single<T>): Single<T> {
        return getter.invoke(appDatabase)
            .onErrorResumeNext { error ->
                Single.error(
                    toDeterminedError(
                        error,
                        ExpectedTrouble.ExpectedCode.FAILED_TO_LOAD_DATA
                    )
                )
            }
    }

    fun modify(
        databaseEvents: Set<KClass<out BaseEntity>>,
        modifier: AppDatabase.() -> Completable
    ): Completable {
        return modifier.invoke(appDatabase)
            .onErrorResumeNext {
                Completable.error(
                    toDeterminedError(
                        it,
                        ExpectedTrouble.ExpectedCode.FAILED_TO_SAVE_CHANGES
                    )
                )
            }
            .doOnComplete {
                if (databaseEvents.isNotEmpty()) {
                    appDatabase.publishChange(databaseEvents)
                }
            }
    }

    fun clear(): Completable {
        return Completable.fromCallable {
            appDatabase.clearAllTables()
            appDatabase.query(SimpleSQLiteQuery("DELETE FROM sqlite_sequence"))
        }
    }
}